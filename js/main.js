require.config({
    // 基础路径，相对于js根开始的
    baseUrl: "js/modules",
    // 这里是需要配置写的模块，不然主目录是找不到的，没有这么智能，这里是配置模块的路径
    paths: {
        // 后缀.js可以不用写
        // module1: './modules/module1',
        // module2:"./modules/module2",
        module1: 'module1',
        module2:"module2"
    }
})
// 这样在主目录下配置了如上的路径才可以找到，这样引入是不可能执行的
// 浏览器里面不认识require这个东西
require(['module2'], function(module2) {`r`
    module2()
})
var browsertify = require("browsertify")
console.log("66666",browsertify)