<!-- 
study address:
https://www.bilibili.com/video/BV1rr4y1Q7Dx?p=5&spm_id_from=pageDriver

目前学的知识是amdJS,最终写的功能要在浏览器页面中使用

define()用来定义模块的，里面写的是什么，返回的就是什么 
具人本见modules中的js中使用

第三方库require.js中也存在

-->

<!-- 
commonJS是依赖nodejs环境来实现的
amdJS是依赖于浏览器端，不依赖于nodejs环境
libs是库目录，主要是放第三方的库
 -->

 #### 暴露模块
 > 没有依赖的写法
 ```js
define(function() {
    return 模块
})
 ```
 > 有依赖的写法
 ```js
 // 把依赖写到数组里在 
define(['module1','module2'], function(m1,m2) {
    return 模块
})
 ```

 #### 引入模块
```js
 require(["module1","module2"], function(m1,m2) {
   使用 m1 / m2  
 })
```
#### browserify use
```js
npm install -g browserify
/*这篇文档用以说明如何使用browserify来构建模块化应用

browserify是一个编译工具,通过它可以在浏览器环境下像nodejs一样使用遵循commonjs规范的模块化编程.

你可以使用browserify来组织代码,也可以使用第三方模块,不需要会nodejs,只需要用到node来编译,用到npm来安装包.

browserify模块化的用法和node是一样的,所以npm上那些原本仅仅用于node环境的包,在浏览器环境里也一样能用.

现在npm上有越来越多的包,在设计的时候就是想好既能在node环境下用,也能在浏览器环境下用的.甚至还有很多包就是给浏览器环境使用的. npm是为所有的javascript服务的,无论前端后端.*/
```